*** Settings ***
Documentation      Robot Framework test script
Library            SSHLibrary
Library            Process
Library            Collections
Library            String

*** Variables ***
${host}            
${username}        user
${password}        user
${alias}           remote_host_1

*** Test Cases ***
Sanity check testcase
    Open Connection     ${host}        alias=${alias}
    Login               ${username}    ${password}  
    Put Directory   /home/user/apertis-tests    /home/user/    recursive=True
    write   cd apertis-tests/common/
    write   sudo ./sanity-check
    ${cmd_out} =    Read    delay=2s  #delay keyword is for reading complete output
    Should Contain   ${cmd_out}   Sanity check successful
    Log To Console   ${cmd_out}    
    Close All Connections

